package br.com.talentos.estudantes.ti.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class WebSecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .httpBasic()
                .and()
                .authorizeHttpRequests()
				.antMatchers(HttpMethod.GET, "/versao/**").permitAll()
				.antMatchers(HttpMethod.GET, "/estudantes/**").permitAll()
				// .antMatchers(HttpMethod.POST, "/estudantes/**").permitAll()
                .antMatchers(HttpMethod.GET, "/swagger-ui.html/**").hasRole(Constants.PERFIL)
                .antMatchers(HttpMethod.POST, "/estudantes/**").hasRole(Constants.PERFIL)
                .antMatchers(HttpMethod.PUT, "/estudantes/**").hasRole(Constants.PERFIL)
                .antMatchers(HttpMethod.DELETE, "/estudantes/**").hasRole(Constants.PERFIL)
                .anyRequest().authenticated()
                .and()
                .csrf().disable();
        return http.build();
    }
}
